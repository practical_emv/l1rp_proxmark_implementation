This code is an update of the proxmark repository to implement the `nonce_rdr`
and `nonce_sim` commands.

Once installed, the code can be tested using two Proxmarks, one to act as the
reader and the other to simulate the card. The reader command will also work with
a normal card as for them the flag in ATQA is not set.

Source code for Proxmark firmware and client which contains two extra commands:
  * `hf 14a noncerdr` -- this behaves as a reader and executes the L1RP protocol 
    up to the RATS message. If bit 9 of the ATQA message is set it carries out 
    the nonce exchange
  * `hf 14a noncesim` -- this simulates a card which expects to do a nonce 
    exchange and so sets bit 9 in ATQA

