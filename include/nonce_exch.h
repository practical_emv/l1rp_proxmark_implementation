/****************************************************************************** 
* File:        nonce_exch.h
* Description: Defines for the addition of nonce exchanges to the ISO14443A
*              protocol`
*
* Author:      Chris Newton
* Created:     Friday 19 June 2020
*
*
******************************************************************************/

#ifndef NONCE_EXCH_H
#define NONCE_EXCH_H

// New command for nonce exchange
#define ISO14443A_CMD_NONCE_READER 0x99
//#define ISO14443A_CMD_NONCE_TAG 0x9a, unused now

// Flag for proprietary bits to indicate nonce exchange
#define ATQA_NONCE_EXCH 0x01

// If this is changed, update the help in CmdHF14ANonceRdr
#define RDR_NONCE_SIZE 4
// If this is changed, update the help in CmdHF14ANonceSim
#define CARD_NONCE_SIZE 4

#endif
